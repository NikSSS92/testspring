package ru.sub.springcourse;

import org.springframework.beans.BeansException;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestSpring {
    public static void main(String[] args) throws BeansException {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

       Music music1 = context.getBean("rockMusic", Music.class);
       Music music2 = context.getBean("classicalMusic", Music.class);

//
       MusicPlayer musicPlayer = context.getBean("musicPlayer", MusicPlayer.class);
       musicPlayer.playMusic();

        context.close();
    }
}
